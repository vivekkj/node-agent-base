# Installation
> `npm install --save @types/agent-base`

# Summary
This package contains type definitions for agent-base (https://github.com/TooTallNate/node-agent-base#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/agent-base.

### Additional Details
 * Last updated: Fri, 15 May 2020 04:08:41 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Christopher Quadflieg](https://github.com/Shinigami92).
